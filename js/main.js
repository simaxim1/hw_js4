/*

Теоретичні запитання:

  1.Описати своїми словами навіщо потрібні функції у програмуванні.

    Для опису процессу та виконання операцій, які, у багатьох випадках, використовуються багаторазово. 
    Це спрощює роботу, зменьшує об'єм коду, підвищує єффективність процесу.

  
  2.Описати своїми словами, навіщо у функцію передавати аргумент.

    Функція формується з параметрами, але працює/виконується з доданими аргументами для отримання необхідного результату.

  
  3.Що таке оператор return та як він працює всередині функції?

    Це оператор, який повертає отриманий результат виконання дії для подальшого використання.

*/

let numberOne;
let numberTwo;
let mathSymbol;

console.log("The result is : " + mathCalc(numberOne,numberTwo,mathSymbol));

function mathCalc (firstNumber,secondNumber,symbol) {
    
  while (!isFinite(firstNumber) | !isFinite(secondNumber)) {
    firstNumber  = prompt("Add number 1", firstNumber);
    secondNumber = prompt("Add number 2", secondNumber);

  };

  while (symbol !=='+' && symbol !=='-' && symbol !=='*' && symbol !='/') {
    symbol = prompt("Add math symbol: +, -, *, /",symbol);
  
  };

  if (symbol + +secondNumber=== "/0") return alert("Can't divide by 0. Please, select another number 2 or math operation.Try again.");
  if (symbol === "+")  return +firstNumber + +secondNumber;
  if (symbol === "-")  return +firstNumber - +secondNumber;
  if (symbol === "*")  return +firstNumber * +secondNumber;
  if (symbol === "/")  return +firstNumber / +secondNumber;

}